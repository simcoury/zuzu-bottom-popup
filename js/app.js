var timeInt, maxTime = convertToMS(3, 20);

function convertToMS(h,m,s){
    m = typeof m!="undefined" ? m : 0;
    s = typeof s!="undefined" ? s : 0;

    var hms, mms, sms;

    sms = s * 1000;
    mms = m * (60 * 1000);
    hms = h * (60 * 60 * 1000);

    return hms + mms + sms;
} //end convertToMS

function convertToTime(ms){
    var h, m ,s, 
        _h = 60 * 60 * 1000,
        _m = 60 * 1000;

    h = Math.floor(ms/_h);
    m = Math.floor((ms-(h*_h))/_m);

    var totalms = (h*_h) + (m*_m);
    s = (ms - totalms)/1000;
    
    return h + "h " + m + "m " + s + "s";
} //end convertToProperTime

function removeTimer(){
    clearInterval(timeInt);
    $("#customPopup .timer").hide();
    $("#customPopup .timer>b").html('');
} //end removeTimer

function checkAtBottomScroll(){
    $(window).scroll(function() {
        if( $(window).scrollTop() + $(window).height() == $(document).height()) {
            showPopup();
        }
     });
} //end checkAtBottomScroll

function showPopup(){
    //check if the popup is enabled
    if( $("#customPopup").hasClass("active") ){
        return;
    }
    $("#customPopup").addClass("active");
} //end showPopup

function checkEmail(str) {
    var regex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return regex.test(str);
} //end checkEmail

$(document).ready(function(){

    //event listener for scroll
    checkAtBottomScroll();

    //validate form (without $.validate plugin)
    $("#frmSample").on("submit", function(e){
        var email = $("#frmSample [name='email']").val();
        
        if( checkEmail(email) ){
            alert("You email - "+email+" - is valid!");

            //assumption, if subscribed already remove the timer
            removeTimer();
        }else{
            alert("You email - "+email+" - is not valid!");
        }

        e.stopPropagation();
        e.preventDefault();
    });

    $("#customPopup .close").on("click", function(){
        $("#customPopup").removeClass("active");
    });

    timeInt = setInterval(function(){ 
        maxTime = maxTime - 1000;
        $("#customPopup .timer>b").html( convertToTime(maxTime) );

        if( maxTime==0 )
            removeTimer();
     }, 1000);
});